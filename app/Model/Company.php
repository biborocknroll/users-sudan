<?php

class Company extends AppModel {

	public $hasMany = [
		'User' => [
			'className' => 'User',
		],
	];

	public function getCompanies(){
		return $this->find('list', [
				'fields' => [
					'Company.id',
					'Company.name',
				],
				'conditions' => [
					'not' => [
						'Company.deleted_flag' => true
					],
				],
			]
		);
	}
}
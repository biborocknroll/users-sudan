<?php
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');
App::uses('AppModel', 'Model');

class User extends AppModel {

	public $belongsTo = 'Company';
	public $hasMany = [
		'Order' => [
			'className' => 'Order',
		],
	];

	public $validate = [
		'User.company_id' => [
			'required' => [
				'rule' => 'notBlank',
				'message' => 'Please choose a company'
			],
		],
		'email_address' => [
			'required' => [
				'rule' => 'notBlank',
				'message' => 'Email Address is required'
			],
			'is_email' => [
				'rule' => 'email',
				'message' => 'Please enter a valid email'
			],
			'is_unique' => [
				'rule' => 'isUnique',
				'message' => 'This email already exists'
			],
		],
		'first_name' => [
			'required' => [
				'rule' => 'notBlank',
				'message' => 'A username is required'
			],
		],
		'contact_number' => [
			'is_numeric' => [
				'rule' => 'numeric',
				'allowEmpty' => true,
				'message' => 'Invalid Contact Number'
			],
		],
		'password' => [
			'required' => [
				'rule' => 'notBlank',
				'message' => 'A password is required'
			],
		],
		're_password' => [
			'is_equal' => [
				'rule' => ['equalTo', 'password' ],
				'message' => 'Password does not match'
			],
			'required' => [
				'rule' => 'notBlank',
				'message' => 'Both password fields must be filled out'
			],
		],
	];

	function equalTo($array, $field) {
		return strcmp($this->data[$this->alias][key($array)], $this->data[$this->alias][$field]) == 0;
	}

	public function beforeSave($options = array()) {
		if (isset($this->data[$this->alias]['password'])) {
			$passwordHasher = new BlowfishPasswordHasher();
			$this->data[$this->alias]['password'] = $passwordHasher->hash( $this->data[$this->alias]['password'] );
		}
		return true;
	}

}
<?php

class MenuDetail extends AppModel {

	public $hasOne = 'Food';
	public $hasOne = 'Menu';
	public $hasMany = [
		'OrderDetail' => [
			'className' => 'OrderDetail',
		],
	];
}
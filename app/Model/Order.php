<?php

class Order extends AppModel {

	public $hasOne = [
		'Menu' => [
			'className' => 'Menu',
		],
		'User' => [
			'className' => 'User',
		],
	];
	public $hasMany = [
		'OrderDetail' => [
			'className' => 'OrderDetail',
		],
	];
}
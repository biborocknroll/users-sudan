<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('cake.generic');

		echo $this->Html->css('bootstrap.min.css');
		echo $this->Html->css('https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css');
		echo $this->Html->css('ionicons.min.css');
		echo $this->Html->css('admin.min.css');
		echo $this->Html->css('_all-skin.min.css');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<header class="main-header">
		<a href="index2.html" class="logo">
			<span class="logo-mini"><b>A</b>LT</span>
			<span class="logo-lg"><b>Admin</b></span>
		</a>
		<nav class="navbar navbar-static-top" role="navigation">
			<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
				<span class="sr-only">Toggle navigation</span>
			</a>
			<div class="navbar-custom-menu">
				<ul class="nav navbar-nav">
			  <!-- User Account: style can be found in dropdown.less -->
					<li class="dropdown user user-menu">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<?php echo $this->Html->image('sadness.gif' , array('class' => 'user-image')) ?>
								<span class="hidden-xs">Sadness</span>
						</a>
						<ul class="dropdown-menu">
							<!-- User image -->
							<li class="user-header">
								<?php echo $this->Html->image('sadness.gif', array('class' => 'user-cirle')) ?>
								<p>
									Sadness - Web Developer
									<small>Member since Nov. 2012</small>
								</p>
							</li>
							<li class="user-footer">
								<div class="pull-left">
									<a href="#" class="btn btn-default btn-flat">Profile</a>
								</div>
								<div class="pull-right">

									<?php echo $this->Html->link(
												'Sign out',[
													'controller' => 'admins',
													'action' => 'logout'],
												['class' => 'btn btn-default btn-flat']
											);?>
								</div>
							</li>
						</ul>
					</li>
					<!-- Control Sidebar Toggle Button -->
					<li>
						<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
					</li>
				</ul>
			</div>
		</nav>
	</header>
	<!-- Left side column. contains the logo and sidebar -->
	<aside class="main-sidebar">
		<!-- sidebar: style can be found in sidebar.less -->
		<section class="sidebar">
		<!-- Sidebar user panel -->
			<div class="user-panel">
				<div class="pull-left image">
					<?php echo $this->Html->image('sadness.gif' , array('class' => 'user-cirle')) ?>
				</div>
				<div class="pull-left info">
					<p>Sadness</p>
				</div>
			</div>
			<!-- sidebar menu: : style can be found in sidebar.less -->
			<ul class="sidebar-menu">
				<li class="header">MAIN NAVIGATION</li>
				<!-- <li class="active treeview">
					<a href="#">
						<i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li class="active"><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
						<li><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
					</ul>
				</li> -->
				<!-- <li class="treeview">
					<a href="#">
						<i class="fa fa-files-o"></i>
						<span>Layout Options</span>
						<span class="label label-primary pull-right">4</span>
					</a>
					<ul class="treeview-menu">
						<li><a href="pages/layout/top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
						<li><a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
						<li><a href="pages/layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
						<li><a href="pages/layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
					</ul>
				</li> -->
			<li>
				<?php echo $this->Html->link(
					$this->Html->tag('i', '',[
						'class' => 'glyphicon glyphicon-cutlery']).$this->Html->tag('span', 'Orders'),[
								'controller' => 'foods',
								'action' => 'index'],
						['escape' => false]);?>
			</li>
			<li>
				<?php echo $this->Html->link(
					$this->Html->tag('i', '',[
						'class' => ' glyphicon glyphicon-menu-hamburger']).$this->Html->tag('span', 'Menu'),[
								'controller' => 'menu',
								'action' => 'index'],
						['escape' => false]);?>
			</li>
			<!-- <li class="treeview">
				<a href="#">
					<i class="fa fa-pie-chart"></i>
						<span>Charts</span>
					<i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
					<li><a href="pages/charts/chartjs.html"><i class="fa fa-circle-o"></i> ChartJS</a></li>
					<li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> Morris</a></li>
					<li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i> Flot</a></li>
					<li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i> Inline charts</a></li>
				</ul>
			</li> -->
		<!-- 	<li>
			<a href="pages/calendar.html">
				<i class="fa fa-calendar"></i> <span>Calendar</span>
					<small class="label pull-right bg-red">3</small>
				</a>
			</li> -->
			<!-- <li class="treeview">
				<a href="#">
					<i class="fa fa-share"></i> <span>Multilevel</span>
					<i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
					<li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
					<li>
						<a href="#"><i class="fa fa-circle-o"></i> Level One <i class="fa fa-angle-left pull-right"></i></a>
						<ul class="treeview-menu">
							<li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
							<li>
								<a href="#"><i class="fa fa-circle-o"></i> Level Two <i class="fa fa-angle-left pull-right"></i></a>
								<ul class="treeview-menu">
									<li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
									<li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
								</ul>
							</li>
						</ul>
					</li>
				<li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
				</ul>
			</li>
			<li><a href="documentation/index.html"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
			<li class="header">LABELS</li>
			<li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
			<li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
			<li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li> -->
			</ul>
		</section>
		<!-- /.sidebar -->
	</aside>


	<div class="content-wrapper">
<!-- 	<section class="content-header">
		<h1>
			Dashboard
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</section> -->
		<?php echo $this->fetch('content'); ?>
	</div>

	<footer class="main-footer">
		<!-- <div class="pull-right hidden-xs">
			<b>Version</b> 2.3.0
		</div> -->
		<strong>Copyright &copy; 2015 <a href="#">Admin</a>.</strong> All rights reserved.
	</footer>

	<!-- Control Sidebar -->
	<aside class="control-sidebar control-sidebar-dark">
		<!-- Create the tabs -->
		<ul class="nav nav-tabs nav-justified control-sidebar-tabs">
			<li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
			<li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
		</ul>
		<!-- Tab panes -->
		<div class="tab-content">
		<!-- Home tab content -->
			<div class="tab-pane" id="control-sidebar-home-tab">


			</div><!-- /.tab-pane -->
			<!-- Stats tab content -->
			<div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div><!-- /.tab-pane -->
			<!-- Settings tab content -->
			<div class="tab-pane" id="control-sidebar-settings-tab">
				<form method="post">

				</form>
			</div><!-- /.tab-pane -->
		</div>
	</aside><!-- /.control-sidebar -->
	<!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
	<div class="control-sidebar-bg"></div>
	</div><!-- ./wrapper -->
	<?php
		echo $this->Html->script('jquery.js');
		echo $this->Html->script('jqueryui.js');
	?>
	 <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
	  $.widget.bridge('uibutton', $.ui.button);
	</script>
	<?php
		echo $this->Html->script('bootstrap.min.js');
		echo $this->Html->script('app.min.js');

	?>
</body>
</html>

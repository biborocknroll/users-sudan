<div class="container">
	<div class="row" style="margin-top:20px">
		<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
			<fieldset>
				<h2>Register</h2>
				<hr class="login-border">
				<?php echo $this->Form->create('User', ['novalidate' => true]); ?>
				<div class="form-group">
					<?php
						echo $this->Form->input('User.company_id', array('class' => 'form-control input-lg'), $companies);
						echo $this->Form->input('email_address', array('class' => 'form-control input-lg', 'placeholder' => 'Email Address'));
						echo $this->Form->input('first_name', array('class' => 'form-control input-lg', 'placeholder' => 'First Name'));
						echo $this->Form->input('middle_name', array('class' => 'form-control input-lg', 'placeholder' => 'Middle Name') );
						echo $this->Form->input('last_name', array('class' => 'form-control input-lg', 'placeholder' => 'Last Name'));
						echo $this->Form->input('contact_number', array('class' => 'form-control input-lg', 'placeholder' => 'Contact Number') );
						echo $this->Form->input('password', array('class' => 'form-control input-lg', 'placeholder' => 'Password'));
						echo $this->Form->input('re_password', array('type'=>'password', 'label'=>'Re-Enter Password', 'class'=>'form-control input-lg', 'placeholder' => 'Re-Enter Password'));
					?>
				</div>
				<hr class="login-border">
				<div class="row">
					<div class="col-xs-6 col-sm-6 col-md-6">
						<?php echo $this->Form->button('Sign Up', array('class' => 'btn btn-lg btn-primary btn-block')); ?>
					</div>
				</div>
			</fieldset>
		</div>
	</div>
</div>
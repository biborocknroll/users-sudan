<div class="container">
	<div class="row" style="margin-top:20px">
		<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
			<fieldset>
				<h2>Please Sign In</h2>
				<hr class="login-border">
				<?php
					echo $this->Flash->render('auth');
					echo $this->Form->create('User', ['novalidate' => true]);
				?>
				<div class="form-group">
					<?php
						echo $this->Form->input('email_address', array('class' => 'form-control input-lg', 'placeholder' => 'Email Address'));
						echo $this->Form->input('password', array('class' => 'form-control input-lg', 'placeholder' => 'Password'));
					?>
				</div>
				<hr class="login-border">
				<div class="row">
					<div class="col-xs-6 col-sm-6 col-md-6">
						<?php
							echo $this->Session->flash();
							echo $this->Form->button('Sign In', array('class' => 'btn btn-lg btn-success btn-block'));
						?>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6" style="float:right; position:relative; margin-top:-75px;">
						<?php
							echo $this->html->link('Register', array('controller'=>'users','action'=>'add'), array('class' => 'btn btn-lg btn-primary btn-block'));
						?>
					</div>
				</div>
			</fieldset>
		</div>
	</div>
</div>

